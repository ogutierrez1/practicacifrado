//Cifrado AES
const crypto = require('crypto');
function encode(text, key){
  const mykey = crypto.createCipher('aes-128-cbc', key);
  let mystr = mykey.update(text, 'utf8', 'hex');
  mystr += mykey.final('hex');
  return (mystr);
}

function decode(message, key){
  const mykey = crypto.createDecipher('aes-128-cbc', key);
  let mystr = mykey.update(message, 'hex', 'utf8');
  mystr += mykey.final('utf8');
  return (mystr);
}

const key = "MyUltraSecretKey";
let value = encode("MySecretMessage", key);
console.log(`El mensaje cifrado es: ${value}`);

value = decode(value, key);
console.log(`El mensaje original: ${value}`);

//Cifrado RSA
const NodeRSA = require('node-rsa');
const keyRSA = new NodeRSA({b: 512});

keyRSA.generateKeyPair();

const publicDer = keyRSA.exportKey('public');
const privateDer = keyRSA.exportKey('private');

// console.log(`PublicDer: ${publicDer}`);
// console.log(`PrivateDer: ${privateDer}`);

const text = 'Secret Information ';
const encrypted = keyRSA.encrypt(text, 'base64');

const decrypted = keyRSA.decrypt(encrypted,'utf8');

console.log(`encrypted: ${encrypted}`);
console.log(`decrypted: ${decrypted}`);
